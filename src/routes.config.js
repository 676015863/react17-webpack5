// import { routes as authorizationRoutes } from './pages/authorization';
import { dynamic } from './utils';
import { routes as homeRoutes } from './pages/home';
import { routes as loginRoutes } from './pages/login';
import { routes as manageRoutes } from './pages/manage';

// 管理页面
const NotFound = dynamic(import('./components/not-found'));
const IndexLayout = dynamic(import('./layout/index-layout'));
const WorkspaceLayout = dynamic(import('./layout/workspace-layout'));

const routes = [
  {
    path: '/manage',
    component: WorkspaceLayout,
    meta: { auth: true },
    routes: [
      ...manageRoutes,
      { path: '*', exact: false, component: NotFound }
    ]
  },
  {
    path: '/',
    component: IndexLayout,
    meta: { auth: false },
    routes: [
      ...loginRoutes,
      ...homeRoutes, // 首页
      { path: '*', exact: false, component: NotFound }
    ]
  }
];

export { routes };
