import axios from 'axios';
import { storage, util } from '../utils';
import { message } from 'antd';
import { user } from '../stores';
import { language } from '../language';
import { config } from '../config';

const globalOptions = {
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: 'Basic 123456'
  }
};

export default class BasicService {
  constructor(baseURL = '') {
    this.baseURL = baseURL;
    // 保存token
    if (user.token) {
      this.setToken(user.token);
    }
  }

  get(url, options) {
    return this._request('get', url, null, options);
  }

  post(url, data, options) {
    return this._request('post', url, data, options);
  }

  put(url, data, options) {
    return this._request('put', url, data, options);
  }

  delete(url, options) {
    return this._request('delete', url, null, options);
  }

  setToken(token) {
    console.log('设置token', token);
    storage.local.set('token', token);
    globalOptions.headers['Authorization'] = token;
  }

  _request(method, url, data, options = {}) {
    const headers = Object.assign({}, globalOptions.headers, options.headers);
    const opt = {
      baseURL: this.baseURL,
      withCredentials: true,
      method,
      url: config.apiHost + url,
      data,
      params: Object.assign(options.params || {}, {
        locale: language.getLanguage()
      }),
      headers
    };
    // axios.defaults.withCredentials = true;
    return axios(opt)
      .then(res => {
        if (res.data.code === 1001) {
          message.error('登录失效');
          user.clearUserInfo();
          util.history.push('/');
          return false;
        } else if (res.data.code) {
          // message.error(res.data.message);
          return false;
        }
        return res.data.data || true;
      })
      .catch(err => {
        console.error('err', err);
        return Promise.reject(err);
      });
  }
}
