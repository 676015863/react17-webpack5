import { layout } from './Layout';
import { user } from './User';

export { layout, user };

export default { layout, user };
