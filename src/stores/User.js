import { action, observable, transaction } from 'mobx';
import { storage, util } from '../utils';
import { userService } from '../server';
import { config } from '../config';

/**
 * @desc 存放外部传入的props数据
 */
class User {
  @observable token = storage.local.get('token'); // 外部传入的参数
  @observable info = storage.local.get('user-info'); // 外部传入的参数

  /**
   * 设置用户信息
   * @param {*} info
   * @param {*} token
   */
  @action
  setUserInfo(info) {
    this.info = info;
    storage.local.set('user-info', this.info);
  }

  @action
  getUserInfo() {
    return this.info;
  }

  @action
  getToken() {
    return this.token;
  }

  @action
  setToken(token) {
    this.token = token;
    storage.local.set('token', token);
  }

  @action
  updateUserInfo(values) {
    transaction(() => {
      for (let key in values) {
        this.info[key] = values[key];
      }
      storage.local.set('user-info', this.info);
    });
  }

  @action
  logout = async () => {
    await userService.logout();
    user.clearUserInfo();
    util.history.push(config.unLoginLinkTo);
  };

  @action
  clearUserInfo() {
    transaction(() => {
      this.info = null;
      this.token = null;
    });
    storage.local.remove('token');
    storage.local.remove('user-info');
  }
}

export const user = new User();
