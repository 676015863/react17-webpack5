import { action, observable, transaction } from 'mobx';
import { util, storage } from '../utils';

class Layout {
  @observable layoutKeys = {};

  // 手动触发模块更新
  @action
  updateComponent = (...keyName) => {
    transaction(() => {
      for (let i = 0; i < keyName.length; i++) {
        this.layoutKeys[keyName[i]] = util.randomID();
      }
    });
  };
}

export const layout = new Layout();
