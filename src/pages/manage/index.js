import { dynamic } from '../../utils';

const Manage = dynamic(import('./Manage'));

const routes = [{ path: '', exact: true, component: Manage }];

export { routes };
