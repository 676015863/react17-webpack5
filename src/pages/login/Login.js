import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { util } from '../../utils';
import LoginRegisterBox from '../../components/login/LoginRegisterBox';
import { config } from '../../config';

@withRouter
@inject('user')
@observer
class Login extends Component {
  componentDidMount() {
    console.log('---------', this.props.user.info);
    if (this.props.user.info) {
      util.history.push(config.loginLinkTo);
    }
  }

  render() {
    return (
      <div>
        <button onClick={() => util.history.push('/')}>to home</button>
        <div style={{ width: 800, background: '#fff' }}>
          <LoginRegisterBox />
        </div>
      </div>
    );
  }
}
export default Login;
