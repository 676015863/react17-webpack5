import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import './home.less';
import { language, Intl } from '../../language';
import { inject, observer } from 'mobx-react';
import LoginRegisterBox from '../../components/login/LoginRegisterBox';
import { config } from '../../config';
import { util } from '../../utils';

// console.log(process.env.NODE_ENV);

@withRouter
@inject('user')
@observer
class Home extends Component {
  componentDidMount() {
    console.log('---------', this.props.user.info);
    if (this.props.user.info) {
      util.history.push(config.loginLinkTo);
    }
  }
  render() {
    return (
      <div className="home">
        <LoginRegisterBox />
      </div>
    );
  }
}
export default Home;
