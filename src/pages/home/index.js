import { dynamic } from '../../../src/utils';

const Home = dynamic(import('./Home'));

const routes = [{ path: 'home', exact: true, component: Home }];

export { routes };
