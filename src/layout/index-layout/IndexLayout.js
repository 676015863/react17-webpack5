import './index-layout.less';

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { config } from '../../config';

@withRouter
@inject('user')
@observer
class IndexLayout extends Component {
  componentDidMount() {
    // 未登录状态直接返回首页
    if (this.props.parentRoute.meta.auth && !this.props.user.info) {
      this.props.history.push(config.unLoginLinkTo);
    }
  }

  render() {
    const { match, routes = [] } = this.props;
    const parentPath = match.path;
    return (
      <Switch>
        {routes.map(route => {
          const routeKey = parentPath + route.path;
          return (
            <Route
              key={routeKey}
              path={routeKey}
              render={props => <route.component {...props} routes={route.routes || []} />}
            />
          );
        })}
      </Switch>
    );
  }
}

export default IndexLayout;
