import React from 'react';
import { UserOutlined, PoweroffOutlined } from '@ant-design/icons';
import { user } from '../../stores';

export default function UserPopover() {
  return (
    <ul className="menus-popover">
      <li>
        <UserOutlined />
        &nbsp; 用户中心
      </li>
      <li onClick={user.logout}>
        <PoweroffOutlined />
        &nbsp; 退出登录
      </li>
    </ul>
  );
}
