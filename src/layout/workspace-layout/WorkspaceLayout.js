import './workspace-layout.less';

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { util } from '../../utils';
import { config } from '../../config';
import { Layout, Avatar, Badge, Space, Popover } from 'antd';
import Menus from './Menus';
import UserPopover from './UserPopover';

import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined
} from '@ant-design/icons';

@inject('user')
@observer
class WorkspaceLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false
    };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  componentDidMount() {
    // 未登录状态直接返回首页
    if (this.props.parentRoute.meta.auth && !this.props.user.info) {
      this.props.history.push(config.unLoginLinkTo);
    }
  }

  render() {
    const { Header, Sider, Content } = Layout;
    const { match, routes = [] } = this.props;
    const parentPath = match.path;
    const { info } = this.props.user;
    if (!info) {
      return <div>登录失效！</div>;
    }
    return (
      <Layout className="workspace">
        <Sider
          trigger={null}
          collapsible={true}
          collapsed={this.state.collapsed}
          style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0
          }}>
          <div className="logo">LOGO</div>
          <Menus collapsed={this.state.collapsed} />
        </Sider>
        <Layout className="site-layout" style={{ marginLeft: this.state.collapsed ? 80 : 200 }}>
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {this.state.collapsed ? (
              <MenuUnfoldOutlined className="trigger" onClick={this.toggle} />
            ) : (
              <MenuFoldOutlined className="trigger" onClick={this.toggle} />
            )}
            <Space>
              <Popover content={<UserPopover />} title="" trigger="click" placement="bottomRight">
                <div style={{ cursor: 'pointer' }}>
                  <Badge count={1}>
                    <Avatar icon={<UserOutlined />} />
                  </Badge>
                  &nbsp; &nbsp;
                  <span>{info.nickName}</span>
                </div>
              </Popover>
            </Space>
          </Header>
          <Content
            className="workspace-content"
            style={{
              margin: 24,
              minHeight: window.innerHeight
            }}>
            <Switch>
              {routes.map(route => {
                const routeKey = parentPath + route.path;
                return (
                  <Route
                    key={routeKey}
                    path={routeKey}
                    render={props => <route.component {...props} routes={route.routes || []} />}
                  />
                );
              })}
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default WorkspaceLayout;
