import './style.less';
import './less/icon.less';
import './utils/pubsub';
import './less/antd.less.hack.less';
import './less/initialize.less';

if (module.hot) {
  module.hot.accept();
}

import { Provider } from 'mobx-react';
import React from 'react';
import Routers from './Routers';
import { render } from 'react-dom';
import stores from './stores'; // ...
import zhCN from 'antd/es/locale/zh_CN';
import { ConfigProvider } from 'antd';

// 路由
render(
  <Provider {...stores}>
    <ConfigProvider locale={zhCN}>
      <Routers />
    </ConfigProvider>
  </Provider>,
  document.getElementById('App')
);
