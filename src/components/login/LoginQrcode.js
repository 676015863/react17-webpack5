import React, { Component } from 'react';
// import qrcode from '../../assets/images/mobile-editor.png';
import { loginServer } from './server';
import { util, pubsub } from '../../utils';
import { config } from '../../config';

class LoginQrcode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      sn: ''
    };
  }

  // 轮询监测登录状态
  seekLogin = async sn => {
    this.timer = setTimeout(async () => {
      const res = await loginServer.seekWxLogin(sn);
      console.log('-----------', res);
      if (!res) {
        this.seekLogin(sn);
      } else {
        clearTimeout(this.timer);
        // 获取到token了
        loginServer.setToken(res.token);
        // 2、获取用户详情，设置x-user-info
        const ress = await loginServer.getUserDetail();
        console.log('ress---->', ress);
        if (ress) {
          util.history.push(config.loginLinkTo);
          pubsub.publish('showLoginModal', false);
        }
      }
    }, 3000);
  };

  doQrcode = async () => {
    const res = await loginServer.getWxQrcode();
    this.setState({
      url: res.url,
      sn: res.sn
    });

    // 轮询登录
    this.seekLogin(res.sn);
  };

  componentDidMount() {
    this.doQrcode();
  }

  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  render() {
    const { url } = this.state;
    return (
      <div className="login-register-qrcode">
        <p>
          <i className="webfont icow-weixin-full" />
          微信扫描立即登录
        </p>
        <img style={{ width: 200 }} src={url || 'https://cdn.h5ds.com/static/images/qrcode-loading.png'} alt="" />
      </div>
    );
  }
}

export default LoginQrcode;
