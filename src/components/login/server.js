import BasicService from '../../server/BasicService';
import { config } from '../../config';
import { user } from '../../stores';

class LoginServer extends BasicService {
  // 获取登录的二维码
  getWxQrcode = async () => {
    return await this.get(`/account/login/wqr`);
  };

  // 查询用户是否通过二维码关注
  seekWxLogin = async sn => {
    return await this.get(`/account/login/wset?sn=${sn}`, { unfilter: true });
  };

  // 获取手机验证码
  getRegisterSMS = async data => {
    return await this.post(`/account/sms/register`, data);
  };

  // 验证码
  getCaptcha = () => {
    return `${config.apiHost}/account/img_captcha?t=` + +new Date();
  };

  // 绑定微信-获取二维码
  getBindWeixinCode = async () => {
    return await this.get('/account/bind-weixin/wqr');
  };

  // 微信绑定-结果轮训
  bindWeixinSeek = async sn => {
    return await this.get('/account/bind-weixin/wset?sn=' + sn, { unfilter: true });
  };

  // 发送邮箱验证码
  sendEmailCode = async data => {
    return await this.post(`/account/mail/bind-email`, data);
  };

  // 绑定邮箱
  bindEmail = async data => {
    return await this.post(`/account/bind-email`, data);
  };

  // 绑定手机号 phoneNumber, code
  bindPhone = async data => {
    return await this.post(`/account/bind-mobile`, data);
  };

  // 绑定手机号，发送验证码 mobile  captchaCode
  getCodeBindMobile = async data => {
    return await this.post(`/account/sms/bind-mobile`, data);
  };

  // 找回密码发送手机验证码 mobile captchaCode
  getCodeResetPassword = async data => {
    return await this.post(`/account/sms/recover-password`, data);
  };

  /**
 * {
      "username": "admin",
      "password": "admin",
      "captchaCode": "ffct"
    }
 * @param {*} registerInfo
 */
  register = async registerInfo => {
    return await this.post(`/account/register`, registerInfo);
  };

  // 登录
  login = async params => {
    const res = await this.post(`/account/login`, params);
    if (res) {
      this.setToken(res.token);
    }
    return res;
  };

  oauthLogin = async code => {
    const res = await this.get('/account/login/provider/qq/user', { params: { code } });
    if (res) {
      this.setToken(res.token);
      user.setToken(res.token);
      user.setUserInfo(res.user);
      return res;
    } else {
      return false;
    }
  };

  /**
   * 修改密码
   *{
  "username": "admin",
  "password": "admin",
  "captchaCode": "6udh"
}
   */
  changePassword = async data => {
    return await this.post('/account/change-password', data);
  };

  /**
   * 找回密码
	"mobile": "15983385886",
	"password": "123456",
	"code": "xf7f"
   * @param {*} data
   */
  findPassword = async data => {
    return await this.post('/account/recover-password', data);
  };

  /**
   * 获取用户信息
   */
  getUserDetail = async () => {
    const res = await this.get('/account/detail');
    user.setUserInfo(res);
    return res;
  };
}

export const loginServer = new LoginServer();
