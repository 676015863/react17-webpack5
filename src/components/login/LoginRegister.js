import React, { useState, useEffect } from 'react';
import { Modal } from 'antd';
import LoginRegisterBox from './LoginRegisterBox';
import { user } from '../../stores';
import { util, pubsub } from '../../utils';

export default function LoginRegister({ children }) {
  const [visible, setVisible] = useState(false);

  const showVisible = () => {
    if (!user.info) {
      setVisible(true);
    } else {
      util.history.push(location.pathname);
    }
  };

  useEffect(() => {
    pubsub.subscribe('showLoginModal', mark => {
      if (mark !== undefined) {
        setVisible(mark);
      } else {
        setVisible(true);
      }
    });
    return () => {
      pubsub.unsubscribe('showLoginModal');
    };
  }, []);

  return (
    <>
      {children ? <span onClick={showVisible}>{children}</span> : <a onClick={showVisible}>登录/注册</a>}
      <Modal
        bodyStyle={{ padding: 0 }}
        title={null}
        width={800}
        visible={visible}
        footer={null}
        destroyOnClose={true}
        onCancel={() => setVisible(false)}
      >
        {visible && <LoginRegisterBox />}
      </Modal>
    </>
  );
}
