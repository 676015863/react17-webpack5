import React from 'react';
import { locals } from './language';

/**
 * <Intl name=""/>
 * @param {string} name 字段名称
 * @param {object} data 模板参数：默认是undefined
 * @param {string} type 语言类型，默认是undefined
 */
export default function Intl({ name, data, type }) {
  if (!window.language) {
    window.language = storage.local.get('language') || 'zh-CN';
  }
  if (!locals[window.language][name]) {
    console.error('name', name);
    console.error('locals[window.language]', locals[window.language]);
  }
  let str = locals[type || window.language][name] || 'not found';
  if (data) {
    for (let key in data) {
      str = str.replaceAll(`{{${key}}}`, data[key]);
    }
  }

  return <>{str}</>;
}
