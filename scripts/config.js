const path = require('path');
const internalIp = require('internal-ip');
const resolve = url => path.resolve(__dirname, url);

console.log(internalIp);

module.exports = {
  resolve,
  distPath: resolve('../dist'),
  srcPath: resolve('../src'),
  version: '1.0.0',
  hash: '.[hash:8]', // 是否开启hash配置
  devServer: {
    host: '127.0.0.1', // internalIp.v4.sync(),
    port: 9900,
    inline: true,
    hot: true,
    compress: true,
    disableHostCheck: true,
    historyApiFallback: true, // using html5 router.
    contentBase: resolve('../public'),
    proxy: {}
  }
};
